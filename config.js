const dotenv = require('dotenv');
const path = require('path');
dotenv.config();

const config = {
  brokerOptions: {
    pactBroker: 'https://raryson.pact.dius.com.au',
    pactFilesOrDirs: [path.resolve('pacts')],
    consumerVersion: process.env.BITBUCKET_BUILD_NUMBER,
    pactBrokerToken: process.env.PACT_FLOW_TOKEN
  },
  usersService: {
    port: 8992,
    host: 'http://localhost'
  }
};

module.exports = config;