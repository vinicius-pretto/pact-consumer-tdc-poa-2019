const axios = require("axios");
const config = require("../config");

const getUsers = async () => {
  const url = `${config.usersService.host}:${config.usersService.port}/users`;
  const response = await axios.get(url);
  return response.data.users;
};

module.exports = {
  getUsers
};
