const expect = require("chai").expect;
const path = require("path");
const { Pact } = require("@pact-foundation/pact");
const { like } = require('@pact-foundation/pact').Matchers;
const HttpStatus = require('http-status');
const config = require('../config');
const userService = require('../src/users.service');

describe('Users service', () => {
  const provider = new Pact({
    port: config.usersService.port,
    log: path.resolve(process.cwd(), "logs", "mockserver-integration.log"),
    dir: path.resolve(process.cwd(), "pacts"),
    spec: 1,
    consumer: "WebApp",
    provider: "UsersService",
    pactfileWriteMode: "merge",
  });

  const expectedBody = {
    users: [
      {
        id: '0a27d95d-fdd7-4022-8122-bfbf023e38f6',
        firstName: 'Vinicius',
        lastName: 'Pretto'
      }
    ]
  };

  before(() => provider.setup())
  after(() => provider.finalize())
  afterEach(() => provider.verify())

  describe('GET /users', () => {
    before(async () => {
      const interaction = {
        state: "I have a list of users",
        uponReceiving: "A request for all users",
        withRequest: {
          method: "GET",
          path: "/users",
          headers: {},
        },
        willRespondWith: {
          status: HttpStatus.OK,
          headers: {},
          body: {
            users: [
              {
                id: like('0a27d95d-fdd7-4022-8122-bfbf023e38f6'),
                firstName: like('Vinicius'),
                lastName: like('Pretto')
              }
            ]
          },
        },
      }
      await provider.addInteraction(interaction)
    });

    it("returns all users", async () => {
      const users = await userService.getUsers();
      expect(users).to.eql(expectedBody.users); 
    });
  });
});